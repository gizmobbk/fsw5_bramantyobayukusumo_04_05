class Peserta {
    constructor(nama) {
        this.nama = nama;
        this.skor = 0;
    }
    ambilNama = () => this.nama;

    buatPilihan = pilih => this.pilih = pilih;

    ambilNilai= () => this.skor;

    tambahNilai = skor => this.skor++;;

    ambilPilihan = () => this.pilih;

    acakPilihan = () => {
        const pilihan =['batu','kertas','gunting'];
        this.pilih = pilihan[Math.floor((Math.random() * pilihan.length))];
        return this.pilih;
    }
}

class Aturan {
    ambilPemenang(pemain, komputer) {
        let pemenang;
        // mencari pemenang permainan
        if ((pemain.ambilPilihan() === "batu" && komputer.ambilPilihan() === "kertas") || 
        (pemain.ambilPilihan() === "kertas" && komputer.ambilPilihan() === "gunting")||
        (pemain.ambilPilihan() === "gunting" && komputer.ambilPilihan() === "batu")) {
            pemenang = komputer;
            komputer.tambahNilai();
        } 
        else if ((komputer.ambilPilihan() === "batu" && pemain.ambilPilihan() === "kertas") ||
        (komputer.ambilPilihan() === "kertas" && pemain.ambilPilihan() === "gunting") ||
        (komputer.ambilPilihan() === "gunting" && pemain.ambilPilihan() === "batu")) {
            pemenang = pemain;
            pemain.tambahNilai();
        } 
        else {
            pemenang = 'seri';
        }
        return pemenang;
    }
}

const pemain = new Peserta('pemain');
const komputer = new Peserta('komputer');
const aturan = new Aturan(pemain,komputer);
const pemenang = document.querySelector('img.lawan');
const pemainPilih = document.querySelectorAll('img.pemain');
pemainPilih.forEach((plh) => {
    plh.addEventListener('click', () => {
        let pilihanPemain = pemain.buatPilihan(plh.id);
        let pilihanKomputer = komputer.acakPilihan();

        // mengganti .komputer-pilih menjadi .tombol-gambar jika benilai benar
        const hapusPilihanKomputer = document.querySelector('img.komputer-pilih');
        if (hapusPilihanKomputer) hapusPilihanKomputer.classList.replace('komputer-pilih', 'tombol-gambar');

        // mengganti .tombol-gambar menjadi .komputer-pilih jika pilihanKomputer bernilai sama
        if (pilihanKomputer === 'batu') {
            const komputerPilih = document.querySelector('img.komputer#batu');
            komputerPilih.classList.replace('tombol-gambar', 'komputer-pilih');
        }
        else if (pilihanKomputer === 'kertas') {
            const komputerPilih = document.querySelector('img.komputer#kertas');
            komputerPilih.classList.replace('tombol-gambar', 'komputer-pilih');
        }
        else {
            const komputerPilih = document.querySelector('img.komputer#gunting');
            komputerPilih.classList.replace('tombol-gambar', 'komputer-pilih');
        }
        
        let hasil = aturan.ambilPemenang(pemain, komputer);
        // mencetak pilihan Pemain dan Komputer dalam Console
        console.log('============================================')
        console.log(`Anda telah memilih ${pilihanPemain}`);
        console.log(`Komputer telah memilih ${pilihanKomputer}`);
        // mencetak pemenang permainan
        if (hasil === 'seri') {
            pemenang.setAttribute('src', 'gambar/seri.png');
            console.log('Hasil SERI, tidak ada pemenang');
        }
        else {
            if (hasil.ambilNama() === 'pemain'){
                pemenang.setAttribute('src', 'gambar/pemain-menang.png');
            }
            else {
                pemenang.setAttribute('src', 'gambar/komputer-menang.png');
            }
            console.log(`Pemenangnya adalah ${hasil.nama}`);
        }
        console.log(`${pemain.ambilNilai()} : ${komputer.ambilNilai()}`)
    });
});

// memuat ulang halaman
const mulaiUlang = document.getElementById('mulai-ulang');
mulaiUlang.addEventListener('click', () => {
    location.reload();
});